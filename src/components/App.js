// Dependencies
import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import 'bootstrap';

// Components
import Login from './global/loginView/Login';
import Teacher from './global/teacherView/Teacher';
import Page404 from './global/page404View/Page404';
import NewQuestion from "./global/newQuestionView/NewQuestion";
import QuizList from './global/quizList/QuizList';
import QuizAnswer from './global/quizAnswer/QuizAnswer';
import QuestionModification from './global/questionModification/QuestionModification';

class App extends Component {
  render() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path ="/" component={Login} exact/>
                <Route path ="/teacherView" component={Teacher}/>
                <Route path="/newQuestion" component={NewQuestion}/>
                <Route path="/quizList" component={QuizList}/>
                <Route path="/quizAnswer" component={QuizAnswer}/>
                <Route path="/questionModification" component={QuestionModification}/>
                <Route component={Page404}/>
            </Switch>
        </BrowserRouter>
    );
  }
}

export default App;
