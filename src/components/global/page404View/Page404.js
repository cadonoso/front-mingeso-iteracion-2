import React, { Component } from 'react';
import ContentPage404 from './ContentPage404';
import HeaderPage404 from "./HeaderPage404";
import FooterPage404 from "./FooterPage404";

class Page404 extends Component {
    render() {
        return(
            <div className="Page404">
                <HeaderPage404 />
                <ContentPage404 />
                <FooterPage404/>
            </div>
        );
    };
}

export default Page404;