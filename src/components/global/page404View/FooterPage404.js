import React, { Component } from 'react';
import './page404.css';

class FooterPage404 extends Component {
  render() {
    return (
      <div className="FooterPage404">
        <h1> Desarrolladores </h1>
        <h2> Cristóbal Donoso </h2>
      </div>
    );
  }
}

export default FooterPage404;
