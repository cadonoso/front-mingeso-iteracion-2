import React, { Component } from 'react';
import './page404.css';

class HeaderPage404 extends Component {
  render() {
    return (
        <div className="HeaderPage404">
            <h1>
                Page 404 not found
            </h1>
        </div>
    );
  }
}

export default HeaderPage404;
