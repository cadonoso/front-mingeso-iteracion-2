import React, { Component } from 'react';
import './page404.css';


class ContentPage404 extends Component {
  render() {
    return (
      <div className="ContentPage404">
          <h1>
              Esta pagina demuestra que ingresaste a una vista que no existe!
          </h1>
      </div>
    );
  }
}

export default ContentPage404;
