import React, { Component } from 'react';
import './teacherView.css';
import usachLogo from './images/usach.png';

class ContentTeacherView extends Component {
  render() {
    return (
      <div id="ContentTeacherView">
          <img id="imagenUsach" src={usachLogo}/>
      </div>
    );
  }
}

export default ContentTeacherView;
