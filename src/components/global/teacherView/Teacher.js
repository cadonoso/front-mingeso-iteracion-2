import React, { Component } from 'react';
import ContentTeacherView from './ContentTeacherView';
import HeaderTeacherView from "./HeaderTeacherView";
import FooterTeacherView from "./FooterTeacherView";
import './teacherView.css';

class Teacher extends Component {
    render() {
        return(
            <div className="Teacher">
                <HeaderTeacherView />
                <ContentTeacherView />
                <FooterTeacherView />
            </div>
        );
    };
}

export default Teacher;