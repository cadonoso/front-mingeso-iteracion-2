import React, { Component } from 'react';
import './teacherView.css';

class FooterTeacherView extends Component {
  render() {
    return (
      <div id="footerViewTeacher" className="FooterTeacherView">
          <h1> Desarrolladores </h1>
          <h2> Cristóbal Donoso </h2>
          <h2> Fabian Lobos </h2>
          <h2> Cristian Sepúlveda </h2>
      </div>
    );
  }
}

export default FooterTeacherView;
