import React, { Component } from 'react';
import './teacherView.css';
import ButtonsElementsTeacherView from './ButtonsElementsTeacherView';

class NavBarElementsTeacherView extends Component {
  render() {
    return (
        <div className="NavBarElementsTeacherView">
            <nav className="navbar navbar-expand-md navbar-light ">
                <div className="container">
                    <a className="navbar-brand" href="/">
                        <i className="fas fa-home"> </i>
                    </a>
                    <ButtonsElementsTeacherView />
                </div>
            </nav>
        </div>
    );
  }
}

export default NavBarElementsTeacherView;
