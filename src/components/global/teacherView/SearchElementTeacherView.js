import React, { Component } from 'react';
import './teacherView.css';

class SearchElementTeacherView extends Component {
  render() {
    return (
        <div className="SearchElementTeacherView">
            <form className="form-inline my-2 my-lg-0 ">
                <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Buscar Pokemón"/>
                    <button className="btn btn-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    );
  }
}

export default SearchElementTeacherView;
