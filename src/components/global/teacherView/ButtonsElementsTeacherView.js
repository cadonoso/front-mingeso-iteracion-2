import React, { Component } from 'react';
import './teacherView.css';

class ButtonsElementsTeacherView extends Component {
  render() {
    return (
        <div className="ButtonsElementsTeacherView">
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                <span className="navbar-toggler-icon"> </span>
            </button>
            <div className="navbar-collapse collapse" id="navbarResponsive">
                <ul className="navbar-nav ml-auto">

                    <li className="nav-item">
                        <a type="submit" className="btn btn-dark"  href="/newQuestion"> Ingresar pregunta </a>
                    </li>

                    <li className="nav-item">
                        <a type="button" className="btn btn-dark" href="/quizList"> Responder quiz </a>
                    </li>

                    <li className="nav-item">
                        <a type="button" className="btn btn-dark" href="/questionModification"> Modificar quiz </a>
                    </li>
                </ul>
            </div>
        </div>
    );
  }
}

export default ButtonsElementsTeacherView;
