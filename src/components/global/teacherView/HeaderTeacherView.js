import React, { Component } from 'react';
import './teacherView.css';

import NavBar from './NavBarElementsTeacherView';

class HeaderTeacherView extends Component {
  render() {
    return (
        <div className="HeaderTeacherView">
            <NavBar />
        </div>
    );
  }
}

export default HeaderTeacherView;
