import React, { Component } from 'react';
import Axios from 'axios';
import './newQuestion.css';
import { array } from 'prop-types';

class ContentNewQuestion extends Component {

    constructor(props){
        super(props);

        this.state = {
            codeBody: '',
            pozo: 1,
            variables: [],
        }
    }
    
    getValuesVariables(e){
        e.preventDefault(); 

        var tipoVariable = document.getElementById('selectVariableType').value;
        var nombreVariable = document.getElementById('inputName').value;
        var limiteSuperiorVariable = document.getElementById('inputHighRange').value;
        var limiteInferiorVariable = document.getElementById('inputLowRange').value;

        if (tipoVariable === '' || nombreVariable === '' || limiteSuperiorVariable === '' || limiteInferiorVariable === '') {
            window.alert("Faltan datos o han sido ingresados incorrectamente!");
        } else {
            if(parseInt(limiteInferiorVariable, 10) < -100 || parseInt(limiteSuperiorVariable, 10) > 100){
                window.alert("Los límites están fuera de rango, por favor ingrese límites entre 100 y -100.");
            }
            if(parseInt(limiteInferiorVariable, 10) > parseInt(limiteSuperiorVariable, 10)){
                window.alert("El límite superior debe ser mayor o igual al límite inferior.");
            }
            else{
                var flag = 1;
                for (let i = 0; i < this.state.variables.length; i++) {
                    if(this.state.variables[i].name === nombreVariable){
                        flag = 0;
                    }
                }
                if(flag === 1){
                    var arrayDatosOfVariable = {
                        dataType: tipoVariable,
                        lowerNumberLimit: parseInt(limiteInferiorVariable, 10),
                        name: nombreVariable,
                        upperNumberLimit: parseInt(limiteSuperiorVariable, 10),
                    }
                    
                    this.setState({
                        variables: this.state.variables.concat([arrayDatosOfVariable])
                    },(e) => { window.alert("Variable añadida exitosamente!");
                    });
                }
                else{
                    window.alert("No se puede añadir una variable con el mismo nombre!");
                } 
            }   
        }
    }

    getInformation(e){
        e.preventDefault(); 

        if(this.state.variables.length === 0){
            alert("Faltan datos o han sido ingresados incorrectamente!");
        } else {
            var codeText = document.getElementById('codeTextPython').value;
            codeText = this.corregirCodeText(codeText);
            
            this.setState({
                codeBody: codeText
            }, () => {this.getPozo(e);
            });
        }   
    }

    corregirCodeText(codeText){
        codeText = codeText.replace(/"/g, "'");
        return codeText;
    }

    getPozo(e){
        e.preventDefault(); 

        var pozoValue = document.getElementById('selectQuizNumber').value;
        
        
        this.setState({
            pozo: parseInt(pozoValue, 10),
        }, () => {this.sendPostMethod();
        });
    }

    sendPostMethod(){

        let axiosConfig = {
            headers: {
              'Content-Type': 'application/json;charset=UTF-8',
               "Access-Control-Allow-Origin": "@crossorigin"
           }    
        };
        var stateVar = {
            codeBody: this.state.codeBody,
            pozo: this.state.pozo,
            variables: this.state.variables,
        }
        console.log(stateVar);
        Axios.post('http://159.89.121.201:8081/Proyecto-Sprint2/question/add', stateVar, axiosConfig)
        .then(res => {
            console.log(res.status);
            if(res.status === 200){
                window.alert("La pregunta ha sido añadida correctamente!");
                window.location = "/teacherView"
            }
        },

            (error) => {
                window.alert("Error al agregar pregunta, datos incorrectos o incompletos.");
        }

        );
    }

    handleSubmit = (e) =>{
        e.preventDefault();
        
    }

    render() {

    return (
        <div className="ContentNewQuestion">

            <div className="row">
                <div className="form-group col-md-3">
                    <label> Seleccione Quiz </label>
                    <select id="selectQuizNumber" className="form-control">
                        <option defaultValue>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                        <option>8</option>
                        <option>9</option>
                    </select>
                </div>
            </div>

            <br/>
            <div id="addVariablesContent">
                <label id="titleAddVariablesContent"> Ingreso de variables </label>
                <div className="row">
                    <div className="col-md-3">
                        <label> Tipo de variable </label>
                        <select id="selectVariableType" className="form-control">
                            <option defaultValue>INT</option>
                            <option>LONG</option>
                            <option>FLOAT</option>
                            <option>COMPLEX</option>
                            <option>bool</option>
                            <option>str</option>
                            <option>list</option>
                            <option>tuple</option>
                            <option>range</option>
                            <option>xrange</option>
                            <option>dict</option>
                            <option>set</option>
                            <option>frozenset</option>
                            <option>noneType</option>
                            <option>file</option>
                        </select>
                    </div>

                    <div className="col-md-3">
                        <div className="form-group">
                            <label> Nombre variable</label>
                            <input type="text" className="form-control" id="inputName" placeholder="example: variable1" name="variableName"/>
                        </div>
                    </div>

                    <div className="col-md-3">
                        <div className="form-group">
                            <label> Límite superior</label>
                            <input type="text" className="form-control" id="inputHighRange" placeholder="example: 100"/>
                        </div>
                    </div>

                    <div className="col-md-3">
                        <div className="form-group">
                            <label> Límite inferior</label>
                            <input type="text" className="form-control" id="inputLowRange" placeholder="example: -100"/>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-3 col-md-6 col-md-12">
                        <button id="addVariable" className="btn btn-dark" onClick={(e) => {this.getValuesVariables(e)}}>
                            <i className="far fa-plus-square"> </i>
                        </button>
                    </div>
                </div>

                <div id="contenedorVariables">
                    <ul>
                        <div className="row">
                            {this.state.variables.map((post, index) => {
                                return(
                                    <div id = "containerOneVariable" className="col-md-4">
                                        <div className="row">
                                            <label id="questionTittleNewQuestion"> Tipo de dato: {post.dataType}</label>
                                        </div>
                                        <div className="row">
                                            <label id="questionTittleNewQuestion"> Nombre variable: {post.name}</label>
                                        </div>
                                        <div className="row">
                                            <label id="questionTittleNewQuestion"> Límite superior: {post.upperNumberLimit}</label>
                                        </div>
                                        <div className="row">
                                            <label id="questionTittleNewQuestion"> Límite inferior: {post.lowerNumberLimit}</label>
                                        </div>
                                    </div>
                                )
                                })
                            }
                        </div>
                    </ul>
                </div>
            </div>

            <br/>

            <div id="addCodeContent">
                <label htmlFor="addVariablesContent" id="titleAddVariablesContent"> Ingresar código python </label>
                <div className="form-group" id="textAreaCode">
                    <textarea id="codeTextPython" className="form-control" rows="20"></textarea>
                </div>
            </div>

            <div id="addVariablesContent">
                <div className="row">
                    <div className="col-md-4">
                        <a id="backButton" type="submit" className="btn" href="/teacherView">
                            Volver atras
                        </a>
                    </div>

                    <div className="col-md-8">
                        <a id="submitQuestionButton" type="submit" className="btn" onClick={(e) => {this.getInformation(e)}} >
                            Guardar
                        </a>
                    </div>
                </div> 
            </div>
        </div>

    );
  }
}

export default ContentNewQuestion;
