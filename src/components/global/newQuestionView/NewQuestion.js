import React, { Component } from 'react';
import ContentNewQuestion from './ContentNewQuestion';
import './newQuestion.css';

class NewQuestion extends Component {
    render() {
        return(
            <div className="NewQuestion">
                <ContentNewQuestion />
            </div>
        );
    };
}

export default NewQuestion;