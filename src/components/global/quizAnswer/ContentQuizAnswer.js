import React, { Component } from 'react';
import './ContentQuizAnswer.css';
import Countdown from 'react-countdown-now';
import axios from 'axios';

import Axios from 'axios';

class ContentQuizAnswer extends Component {

  constructor(props){
    super(props);

    this.state = {
        notaQuiz: '',
        quizNumber: '',
        codeBodyAux: 'a=0\nprint(a)',
        questions: [],
        answers: [],  
        summary: [],
    }

    this.getQuestions();
  }

  getQuestions(){
    var hashURL = window.location.hash;
    hashURL = hashURL.substr(1);
    
    Axios.get('http://159.89.121.201:8081/Proyecto-Sprint2/quiz/' + hashURL)
    .then(res => {
      console.log(res.status);
      var largoArray = res.data.questions.length;
      var i;
      var newQuestion;
      for (i = 0; i < largoArray; i++) {
        newQuestion = {
          questionNumber: i+1,
          codeBody: res.data.questions[i].codeBody,
        }
        this.setState({
          quizNumber: hashURL,
          questions: this.state.questions.concat([newQuestion])
        });
      }
      });
    }

  loadSummary(e){
    e.preventDefault();
    var flag = window.confirm("¿Está seguro que desea enviar sus respuestas?");
    if(flag){
      var stateVar = {
        codeBody: [],
        respuestaAlumno: [],
      }
      

      var numberOfQuestions = this.state.questions.length;
      var answersArrayAux;
      var answerArray = [];
      var codeBodyAux = {
        code: '',
      }

      var respuestaAlumnoAux = {
        respuesta: '',
      }
      var i;
      console.log(numberOfQuestions);
      for (i = 1; i <= numberOfQuestions; i++) {
        var indexAnswer = 'respuesta_' + i;
        var answerCapted = document.getElementById(indexAnswer).value;

        console.log(this.state.questions[i-1].codeBody);

        codeBodyAux = {
          code: this.state.questions[i-1].codeBody,
        }

        respuestaAlumnoAux = {
          respuesta: answerCapted,
        }

        stateVar = {
          codeBody: stateVar.codeBody.concat([codeBodyAux]),
          respuestaAlumno: stateVar.respuestaAlumno.concat([respuestaAlumnoAux]),
        }
        
        answersArrayAux = {
          questionNumber: i,
          answerInput: answerCapted,
        }

        answerArray = answerArray.concat([answersArrayAux]);

        console.log(answersArrayAux);
        console.log(stateVar);
        
      }



      console.log(answerArray);
      this.setState({
        answers: answerArray,
      },() => {this.changeViews(e, stateVar);
      });
    }
    else{
      // No se hace nada
    }
  }

  changeViews(e, stateVar){
    e.preventDefault();

    let axiosConfig = {
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
         "Access-Control-Allow-Origin": "@crossorigin"
      }
    };

    axios.post('http://159.89.121.201:8081/Proyecto-Sprint2/quizalumno/reviewQuiz', stateVar, axiosConfig)
        .then(res => {
          console.log(res);
          for (let i = 0; i < res.data.codeBody.length; i++) {
            var newResult = {
              questionNumber: i+1,
              question: res.data.codeBody[i].code,
              answer: res.data.respuestaAlumno[i].respuesta,
              correctAnswer: res.data.respuestaCorrecta[i].respuesta,
            }

            this.setState({
              notaQuiz: res.data.nota + '.0',
              summary: this.state.summary.concat([newResult])
            });
          }
          
        });

    var boxOfQuiz = document.getElementById('containerOfQuestions');
    var boxOfSummary = document.getElementById('containerOfSummary');
    boxOfQuiz.style.display = 'none';
    boxOfSummary.style.display = 'block';
  }

  render() {
    return (
      <div id="ContentQuizAnswer">
      
        <h3>Rindiendo quiz: {this.state.quizNumber}</h3>

        

        <div id="containerOfQuestions">

          <label id="timeRemaining">
                Tiempo restante = <Countdown date={Date.now() + 900000} />
          </label>

          <ul>
            {this.state.questions.map((post, index) => {
              return(
                <div id = {"containerOneQuestion_" +  post.questionNumber}>
                  <div className="row">
                    <label id="questionTittle"> Pregunta: {post.questionNumber}</label>

                      <div id={"textOfQuestion_" + post.questionNumber}>
                        <label id={"textoPregunta_" + post.questionNumber}> {post.codeBody}</label>
                      </div>
                  </div>

                  <div className="row">

                    <div className="col-md-2">
                      <label id={"ingresePreguntaText_" + post.questionNumber}> Ingrese respuesta</label>
                    </div>

                    <div className="col-md-10">
                      <div class="form-group">
                        <input type="text" class="form-control" id={"respuesta_" + post.questionNumber}/>
                      </div>
                    </div>

                  </div>

                </div>
              )
            })
            }
          </ul>
          <a  id="answerQuizBackButton" className="btn btn-dark" onClick={(e) => {this.loadSummary(e)}}> Enviar </a>
        </div>

        <div id = "containerOfSummary">
        
            <ul>
              {this.state.summary.map((post, index) => {
                return(
                  <div id = {"containerOfOneResult_" +  post.questionNumber}>
                    <div className="row">
                      <div className="col-md-4">
                        <label id="questionTittle"> Pregunta: {post.question}</label>
                      </div>  

                      <div className="col-md-8">
                        
                      </div>
                        
                    </div>

                    <div className="row">
                        <label id={"respuestaIngresadaSummary_" + post.questionNumber}> Respuesta ingresada: {post.answer}</label>
                    </div>

                    <div className="row">
                        <label id={"respuestaCorrectaSummary_" + post.questionNumber}> Respuesta correcta: {post.correctAnswer} </label>
                    </div>

                  </div>
                )
                })
              }
            </ul>

            <div id = "ResultadoNotaQuiz">
              <label id = "textoResultadoNotaQuiz">
                Nota quiz: {this.state.notaQuiz}
              </label>
            </div>            

            <a  id="listQuizBackButton" className="btn btn-dark"  href="/quizList"> Aceptar </a>
        </div>
      </div>
    );
  }
}

export default ContentQuizAnswer;
