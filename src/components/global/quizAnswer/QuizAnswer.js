import React, { Component } from 'react';
import ContentQuizAnswer from './ContentQuizAnswer';

class QuizAnswer extends Component {
    render() {
        return(
            <div className="QuizAnswer">
                <ContentQuizAnswer />
            </div>
        );
    };
}

export default QuizAnswer;