import React, { Component } from 'react';
import ContentLogin from './ContentLogin';
import HeaderLogin from "./HeaderLogin";
import FooterLogin from "./FooterLogin";
import './login.css';

class Login extends Component {
    render() {
        return(
            <div className="Login">
                <HeaderLogin />
                <ContentLogin />
                <FooterLogin />
            </div>
        );
    };
}

export default Login;