import React, { Component } from 'react';
import './login.css';

import userImage from './images/user.jpg';

class ContentLogin extends Component {

  render() {
    return (
        <div className="ContentLogin">
            <div className="modal-dialog text-center">
                <div className="col-sm-8 main-section">
                    <div className="modal-content">
                        <div className="col-12 user-img">
                            <img src={userImage} style={{height: 100, width: 100}}/>
                        </div>

                        <form className="col-12">
                            <div id="usernameInput" className="form-group">
                                <input type="text" className="form-control" placeholder="Username"/>
                            </div>

                            <div id="passwordInput" className="form-group">
                                <input type="password" className="form-control" placeholder="Password"/>
                            </div>

                            <a id="loginButton" type="submit" className="btn" href="/teacherView">
                                <i className="fas fa-sign-in-alt"> </i>
                                Login
                            </a>
                        </form>

                        <div className="col-12 forgot">
                            <a href="#"> Forgot password?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    );
  }
}

export default ContentLogin;
