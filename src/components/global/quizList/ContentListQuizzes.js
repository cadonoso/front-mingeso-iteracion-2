import React, { Component } from 'react';
import './ContentListQuizzes.css';
import Axios from 'axios';

class ContentListQuizzes extends Component {

    constructor(props){
        super(props);

        this.loadListOfQuizzes();

        this.state = {
            quiz: [],
            idQuizSelect: 0,
            cantidadPreguntasQuiz: 0,
        }
    }

    loadListOfQuizzes() {
        Axios.get('http://159.89.121.201:8081/Proyecto-Sprint2/pozo/all')
        .then(res => {
            console.log(res.data);
            var i;
            var largoArrayQuizzes = res.data.length;
            var newQuiz;
            for (i = 0; i < largoArrayQuizzes; i++) {

                newQuiz = {
                    idQuiz: res.data[i].id,
                    descripcion: res.data[i].descripcion,
                    cantidadPreguntasQuiz: res.data[i].cantidadPreguntasQuiz,
                }
                this.setState({
                    quiz: this.state.quiz.concat([newQuiz])
                });
            }

            console.log(this.state);
        });
    }
    
    checkQuiz(e, idQuiz, cantidadPreguntasQuiz){
        e.preventDefault();

        if(cantidadPreguntasQuiz === 0){
            window.alert("No se puede responder este quiz ya que no tiene preguntas.");
        }
        else{
            window.location = "/quizAnswer/#" + idQuiz
        }
        
    }

  render() {

    return (
        <div id = "ContentListQuizzes">
            <h2>Listado de Quizzes</h2>
            <div id = "containerOfQuizzes">
                <ul>
                    {this.state.quiz.map((post, index) => {
                        return(
                            <div id = "containerOfOneQuiz">
                                <div className="row">
                                    <div className="col-md-2" id="tittleQuiz">
                                        <label>Quiz</label>
                                        <label id={"idOfQuiz_" + post.idQuiz}>: {post.idQuiz} </label>
                                    </div>
                                    <div className="col-md-2">
                                        <div className="row">
                                            <label id="infoOfQuiz">Descripción: {post.descripcion} </label>
                                        </div>
                                    </div>

                                    <div className="col-md-3">
                                        <div id="cuadroNota">
                                            Nota obtenida
                                            <label id="notaQuiz"> 7.0 </label>
                                        </div>
                                    </div>

                                    <div className="col-md-3">
                                        <div id="datosQuiz">
                                            <div  id="infoOfQuiz" className="row">
                                                Fecha: 18/06/2018 
                                            </div>
                                            
                                            <div id="infoOfQuiz" className="row">
                                                Hora inicio: 16:00 hrs. 
                                            </div>

                                            <div id="infoOfQuiz" className="row">
                                                Duración: 90 minutos
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-md-2">
                                        <a id={"answerQuizButton_" + post.idQuiz} className="btn btn-dark" onClick={(e) => {this.checkQuiz(e, post.idQuiz ,post.cantidadPreguntasQuiz)}}> Responder </a>
                                    </div>
                                </div>
                            </div>
                        )
                        })
                    }
                </ul>
            </div>

            <a  id="listQuizBackButton" className="btn btn-dark"  href="/teacherView"> Volver atrás </a>
        </div>

    );
  }
}

export default ContentListQuizzes;
