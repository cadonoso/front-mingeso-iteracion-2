import React, { Component } from 'react';
import ContentListQuizzes from './ContentListQuizzes';

class QuizList extends Component {
    render() {
        return(
            <div className="QuizList">
                <ContentListQuizzes />
            </div>
        );
    };
}

export default QuizList;