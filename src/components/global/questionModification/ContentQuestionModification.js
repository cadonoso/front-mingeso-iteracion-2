import React, { Component } from 'react';
import './ContentQuestionModification.css';
import Axios from 'axios';

class ContentQuestionModification extends Component {

    constructor(props){
        super(props);

        this.getAllQuizzes();

        this.state = {
            quizzes: [],
        }
    }  

    getAllQuizzes(){
        Axios.get('http://159.89.121.201:8081/Proyecto-Sprint2/pozo/all')
        .then(res => {
            console.log(res.data);
            var i;
            var largoArrayQuizzes = res.data.length;
            var newQuiz;
            for (i = 0; i < largoArrayQuizzes; i++) {
                newQuiz = {
                    idQuiz: res.data[i].id,
                    descripcion: res.data[i].descripcion,
                    preguntasQuiz: res.data[i].cantidadPreguntasQuiz,
                    preguntasPozo: res.data[i].cantidadPreguntasPoso
                }
                this.setState({
                    quizzes: this.state.quizzes.concat([newQuiz])
                });
            }
        });
    }

    modififyQuiz(e, numberOfQuiz){
        e.preventDefault();

        var boxAux;

        for (let i = 1; i <= this.state.quizzes.length; i++) {
            if(i != numberOfQuiz){
                boxAux = document.getElementById('containerOfOneQuestionToChange_' + i);
                boxAux.style.pointerEvents = 'none';
            }
        }

        var boxOfConfigureButton = document.getElementById('configurateButtonQuiz_' + numberOfQuiz);
        var boxOfdescription = document.getElementById('infoOfQuiz_'  + numberOfQuiz);
        var boxOfQuestionsInQuiz = document.getElementById('cantidadPreguntasQuiz_'  + numberOfQuiz);

        var boxOfCheckButton = document.getElementById('checkButtonQuiz_'  + numberOfQuiz);
        var boxOfInputDescription = document.getElementById('inputDescripcion_'  + numberOfQuiz);
        var boxOfInputQuestionsInQuiz = document.getElementById('inputCantidadPreguntasQuiz_'  + numberOfQuiz);

        boxOfConfigureButton.style.display = 'none';
        boxOfdescription.style.display = 'none';
        boxOfQuestionsInQuiz.style.display = 'none';

        boxOfCheckButton.style.display = 'flex';
        boxOfInputDescription.style.display = 'flex';
        boxOfInputQuestionsInQuiz.style.display = 'flex';


        console.log(numberOfQuiz);
    }

    checkQuizModified(e, numberOfQuiz, questionsInPozo){
        e.preventDefault();

        let axiosConfig = {
            headers: {
              'Content-Type': 'application/json;charset=UTF-8',
               "Access-Control-Allow-Origin": "@crossorigin"
           }    
        };

        var boxOfConfigureButton = document.getElementById('configurateButtonQuiz_' + numberOfQuiz);
        var boxOfdescription = document.getElementById('infoOfQuiz_'  + numberOfQuiz);
        var boxOfQuestionsInQuiz = document.getElementById('cantidadPreguntasQuiz_'  + numberOfQuiz);

        var boxOfCheckButton = document.getElementById('checkButtonQuiz_'  + numberOfQuiz);
        var boxOfInputDescription = document.getElementById('inputDescripcion_'  + numberOfQuiz);
        var boxOfInputQuestionsInQuiz = document.getElementById('inputCantidadPreguntasQuiz_'  + numberOfQuiz);

    
        console.log("Cantidad de preguntas del quiz a ingresar: " + parseInt(boxOfInputQuestionsInQuiz.value, 10));
        console.log("Cantidad de preguntas del pozo: " + questionsInPozo);
        var flag;
        for (let i = 0; i < boxOfInputQuestionsInQuiz.value.length; i++) {
            if(!isNaN(boxOfInputQuestionsInQuiz.value[i])){
                flag = 1;
            }
            else{
                flag = 0;
                i = boxOfInputQuestionsInQuiz.value.length;
            }
        }

        if(flag === 1){
            if(parseInt(boxOfInputQuestionsInQuiz.value, 10) > questionsInPozo){
                window.alert("La cantidad de preguntas del quiz no puede superar a la cantidad de preguntas el pozo.");
            }
            else{
                console.log(stateVar);
                var stateVar = {
                    cantidadPreguntasQuiz: parseInt(boxOfInputQuestionsInQuiz.value, 10),
                    descripcion: boxOfInputDescription.value,
                    id: numberOfQuiz,
                }
                Axios.put('http://159.89.121.201:8081/Proyecto-Sprint2/pozo/' + numberOfQuiz, stateVar, axiosConfig)
                .then(res => {
                    console.log(res.status);
                    if(res.status === 200){
                        window.alert("Quiz modificado con éxito!");

                        var boxAux;
                        for (let i = 1; i <= this.state.quizzes.length; i++) {
                            if(i != numberOfQuiz){
                                boxAux = document.getElementById('containerOfOneQuestionToChange_' + i);
                                boxAux.style.pointerEvents = 'auto';
                            }
                        }
                        this.setState({
                            quizzes: [],
                        },() => {this.getAllQuizzes();
                        });
                        
                    }
                },

                    (error) => {
                        window.alert("Error al modificar quiz.");
                }

                );

                boxOfConfigureButton.style.display = 'flex';
                boxOfdescription.style.display = 'flex';
                boxOfQuestionsInQuiz.style.display = 'flex';

                boxOfCheckButton.style.display = 'none';
                boxOfInputDescription.style.display = 'none';
                boxOfInputQuestionsInQuiz.style.display = 'none';


                console.log(numberOfQuiz);
            }
        }
        else{
            window.alert("Debe ingresar un número entero en la casilla 'Cantidad de preguntas del quiz'.");
        }
        
    }

  render() {
    return (
        <div id="ContentQuestionModification">
            <div id="containerOfQuestionsToChange">
                <div className="row">

                    <div className="col-md-2">
                        <label id="headerDeInformacion">N° de quiz</label>
                    </div>

                    <div className="col-md-4">
                        <label id="headerDeInformacion">
                            Descripción de quiz
                        </label>
                    </div>

                    <div className="col-md-2">
                        <label id="headerDeInformacion"> 
                            Cantidad de preguntas del quiz 
                        </label>
                    </div>

                    <div className="col-md-2">
                        <label id="headerDeInformacion">
                            Cantidad de preguntas del pozo
                        </label>
                    </div>
                
                </div>

                <ul>
                    {this.state.quizzes.map((post, index) => {
                        return(
                            <div id = {"containerOfOneQuestionToChange_" + post.idQuiz}> 
                                <div className="row">   
                                    <div className="col-md-2">
                                        <label id={"numeroDeQuiz_" + post.idQuiz}> 
                                            {post.idQuiz}
                                        </label>
                                    </div>

                                    <div className="col-md-4">
                                        <label id={"infoOfQuiz_" + post.idQuiz}>
                                            {post.descripcion}
                                        </label>

                                        <input id={"inputDescripcion_" + post.idQuiz} type="text" className="form-control" placeholder="Example: Texto de ejemplo."/>
                                    </div>

                                    <div className="col-md-2">
                                        <label id={"cantidadPreguntasQuiz_" + post.idQuiz}> 
                                            {post.preguntasQuiz}
                                        </label>

                                        <input id={"inputCantidadPreguntasQuiz_" + post.idQuiz} type="text" className="form-control" placeholder="Example: 12"/>
                                    </div>

                                    <div className="col-md-2">
                                        <label id={"cantidadPreguntasPozo_" + post.idQuiz}>
                                            {post.preguntasPozo}
                                        </label>
                                    </div>

                                    <div className="col-md-2" id="cssButtonConfigure">
                                        <button id={"configurateButtonQuiz_" + post.idQuiz} onClick={(e) => {this.modififyQuiz(e, post.idQuiz)}}>
                                            <i class="fas fa-cog"></i>
                                        </button>

                                        <button id={"checkButtonQuiz_" + post.idQuiz} onClick={(e) => {this.checkQuizModified(e, post.idQuiz, post.preguntasPozo)}}>
                                            <i class="fas fa-check"></i>
                                        </button>
                                    </div>
                                    
                                </div>
                            </div>
                        )
                        })
                    }
                </ul>

                            
            </div>

            <a  id="listQuizzesModificationBackButton" className="btn btn-dark"  href="/teacherView"> Finalizar </a>
        </div>

    );
  }
}

export default ContentQuestionModification;
