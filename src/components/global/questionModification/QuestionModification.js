import React, { Component } from 'react';
import ContentQuestionModification from './ContentQuestionModification';

class QuestionModification extends Component {
    render() {
        return(
            <div id="QuestionModification">
                <ContentQuestionModification />
            </div>
        );
    };
}

export default QuestionModification;