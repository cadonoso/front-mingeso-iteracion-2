import React from 'react';
import { Route, Switch } from 'react-router-dom';

import App from './components/App';
import TeacherView from './components/global/teacherView/Teacher';
import NewQuestion from '';
import Page404 from './components/global/page404View/Page404';

const AppRoutes = () =>
    <App>
        <Switch>
            <Route path="/teacherView" component={TeacherView}/>
            <Route path="/Home" component={App}/>

            <Route component={Page404}/>
        </Switch>
    </App>;

export default AppRoutes;
